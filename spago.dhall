{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "simplexi"
, dependencies =
    [ "effect", "console", "concur-react", "argonaut", "bulma" ]
, packages =
    ./packages.dhall
, sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
}
