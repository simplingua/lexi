module Lexi 
(runLocal)
where

import Effect.Console

import Bulma.Columns.Columns (column, columns, isMultiline)
import Bulma.Columns.Size (isSize)
import Bulma.Common (ClassName, Color(..), Is(..), Size(..), runClassNames)
import Bulma.Components.Tabs (tabs)
import Bulma.Elements.Title as T
import Bulma.Form.Common (isColor, isRounded, isSize) as F
import Bulma.Form.Input (input) as F
import Bulma.Layout.Layout (HeroColor(..), hero, heroBody, isHeroColor)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM as D
import Concur.React.Props (Props, _type, className, onChange, placeholder, unsafeTargetValue, value)
import Concur.React.Run (runWidgetInDom)
import Data.Argonaut (Json, decodeJson)
import Data.Array as A
import Data.Either (either)
import Data.Maybe (isJust)
import Data.String (Pattern(..), contains, stripPrefix)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Foreign.Object (toUnfoldable) as F
import Prelude (Unit, bind, pure, show, ($), (<$>), (<<<), (<>), (>>=))

type Config = Array (Tuple String String)

runLocal :: Json -> Effect Unit
runLocal j = do
  _ <- log $ show c
  runWidgetInDom "app" $ teaWidget c
  where c = either (\err -> [Tuple err ""]) F.toUnfoldable $ decodeJson j

-- This is like Elm's State
type SearchingOption =
  { name :: String
  }

-- This is like Elm's Action
data FormAction
  = Name String

formWidget :: SearchingOption -> Widget HTML SearchingOption
formWidget form = do
  -- This is like Elm's view function
  res <- D.div'
    [ Name <$> D.input [_type "text", value form.name, unsafeTargetValue <$> onChange, inputStyle, placeholder "cerca lexi ..."]
    , D.div [classes [tabs]] [D.ul' []]
    , D.text "resulta:"
    ]
  -- This is like Elm's update function
  case res of
    Name s -> pure (form {name = s})


teaWidget :: Config  -> forall a. Widget HTML a
teaWidget c = go initForm
  where
    go f = D.div' [formWidget f, showForm c f] >>= go


initForm :: SearchingOption
initForm = {name: ""}

showForm :: forall a. Config -> SearchingOption -> Widget HTML a
showForm c f = D.div [classes [columns, isMultiline]] $ contents <$> r where
  contents (Tuple spell explain) = D.div
    [classes [column, isSize Is6]]
    [D.section [classes [hero, isHeroColor Light]]
      [ D.div [classes [heroBody]]
        [ D.h1 [classes [T.title]] [D.text spell]
        , D.h2 [classes [T.subtitle]] [D.text explain]
        ]
      ]
    ]
  r = sortIt r1.yes <> sortIt r1.no
  r0 = A.take 100 $ A.filter (\(Tuple w1 w2) -> contains (Pattern f.name) $ w1 <> w2) c
  r1 = A.partition (\(Tuple w _) -> isJust $ stripPrefix (Pattern f.name) w) r0
  sortIt = A.sortWith (\(Tuple w _) -> w)


inputStyle :: forall a. Props a
inputStyle = classes [F.input, F.isSize Large, F.isColor Primary, F.isRounded]

classes :: forall a. Array ClassName -> Props a
classes = className <<< runClassNames
